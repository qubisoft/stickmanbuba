﻿using UnityEngine;

public class lineScript : MonoBehaviour
{
    public GameObject putLineHere;
    public bool broken = false;
    
    private float startY;
    private float targetY;
    private int roundDig = 2;

    //private float dist;
    private float speed;

    private GameObject buba;

    public bool stop = false;

    private void Start()
    {
        //dist = putLineHere.GetComponent<putLinesHere>()._params.distBetweenLines;
        speed = putLineHere.GetComponent<putLinesHere>()._params.speed;
        buba = putLineHere.GetComponent<putLinesHere>().bubaObj;
        startY = GameObject.FindGameObjectWithTag(tags.ceiling).transform.position.y;
        targetY = GameObject.FindGameObjectWithTag(tags.stop).transform.position.y;
    }

    private void Update()
    {
        if (!stop)
        {
            Vector2 pos = gameObject.transform.position;
            // go down
            gameObject.transform.position = Vector2.MoveTowards(pos, new Vector2(pos.x, targetY), speed);

            // if broken, run the animation
            if (broken)
                gameObject.GetComponent<Animator>().SetTrigger("broke");

            //// if goes down to target1, create new one
            //if (System.Math.Round(pos.y - (startY - dist), roundDig) == 0)
            //    putLineHere.GetComponent<putLinesHere>().putLineNow();

            // if goes down to target2, destroy it
            if (System.Math.Round(pos.y - targetY, roundDig) == 0)
                Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == tags.nextLine)
            putLineHere.GetComponent<putLinesHere>().putLineNow();
    }
    
}
