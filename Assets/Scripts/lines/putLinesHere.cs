﻿using UnityEngine;

public class putLinesHere : MonoBehaviour
{
    [SerializeField] LineSO[] LineSO;
    [SerializeField] GameObject buba;

    [SerializeField] GameObject paramsObj;
    [SerializeField] GameObject jumpsCounter;
    [SerializeField] GameObject basketFlower;
    [SerializeField] GameObject basketStar;
    [SerializeField] GameObject basketHeart;

    public _params _params;
    public GameObject bubaObj;

    private LineSO[] lines;
    private float startY;
    private int total = 10;
    private float[] startXall;

    private bool start = true;
    public int count = 0;
    private Vector2 putBubaPos;

    void Start()
    {
        _params = paramsObj.GetComponent<_params>();
        startY = GameObject.FindGameObjectWithTag(tags.ceiling).transform.position.y;

        // create starting points
        float startWidth = gameObject.GetComponent<BoxCollider2D>().size.x;
        float startX = gameObject.transform.position.x - startWidth/2;
        startXall = new float[_params.noOfLinesInOneRow];
        for (int i = 0; i < _params.noOfLinesInOneRow; i++)
            startXall[i] = startX + startWidth / (_params.noOfLinesInOneRow - 1) * i;

        // put first regular line
        Vector2 pos = new Vector2(startXall[(int)Random.Range(0, _params.noOfLinesInOneRow)], startY);
        putLine(LineSO[0], pos);

        // get all lines
        lines = new LineSO[total];
        int percNarrow = (int)System.Math.Round(total * _params.percOfNarrowLines, 0);
        int percBroken = (int)System.Math.Round(total * _params.percOfBrokenLines, 0);
        for (int i = 0; i < percNarrow; i++)
            lines[i] = LineSO[1];
        for (int i = percNarrow; i < percNarrow + percBroken; i++)
            lines[i] = LineSO[2];
        for (int i = percNarrow + percBroken; i < total; i++)
            lines[i] = LineSO[0];
    }

public void putLineNow()
    {
        Vector2 pos = new Vector2(startXall[(int)Random.Range(0, _params.noOfLinesInOneRow)], startY);
        putLine(lines[Random.Range(0, total)], pos);
    }

    private void putLine(LineSO lineType, Vector2 position)
    {
        // put game object in random position
        GameObject line = new GameObject();
        line.transform.localPosition = position;
        if (start)
        {
            putBubaPos = position;
            count += 1;
        }

        // add sprite renderer with line image
        line.AddComponent<SpriteRenderer>();
        line.GetComponent<SpriteRenderer>().sprite = lineType.line;

        // add box collider
        line.AddComponent<BoxCollider2D>();
        line.GetComponent<BoxCollider2D>().usedByEffector = true;
        line.AddComponent<PlatformEffector2D>();
        line.GetComponent<PlatformEffector2D>().surfaceArc = 90f;

        // add script
        line.AddComponent<lineScript>();
        line.GetComponent<lineScript>().putLineHere = gameObject;

        // name, tag, layer
        line.name = "line";
        line.tag = lineType.tag;
        line.GetComponent<SpriteRenderer>().sortingLayerName = lineType.layer;

        // scale it 
        if (lineType.shortLine)
            line.transform.localScale = new Vector2(0.5f, 1f);

        // run animation in broken lines
        if (lineType.broken)
        {
            line.AddComponent<Animator>();
            line.GetComponent<Animator>().runtimeAnimatorController = lineType.anim;
        }
    }

    private void Update()
    {
        if (start && count == 1)
        {
            // stop counting
            start = false;

            // create buba
            bubaObj = Instantiate(buba, putBubaPos, Quaternion.identity);
            bubaObj.name = "buba";

            // add parameters
            bubaObj.GetComponent<bubaScript>()._params = paramsObj;
            bubaObj.GetComponent<bubaScript>().jumpsCounter = jumpsCounter;
            bubaObj.GetComponent<bubaScript>().basketFlower = basketFlower;
            bubaObj.GetComponent<bubaScript>().basketStar = basketStar;
            bubaObj.GetComponent<bubaScript>().basketHeart = basketHeart;
        }
    }
}
