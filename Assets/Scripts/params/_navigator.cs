﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class _navigator : MonoBehaviour
{
    [SerializeField] GameObject askTab;

    public void goToScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
    public void exitApp ()
    {
        GameObject canvas = GameObject.FindGameObjectWithTag(tags.menuCanvas);
        GameObject askQ = Instantiate(askTab, Vector3.zero, Quaternion.identity);
        askQ.transform.parent = canvas.transform;
    }
}
