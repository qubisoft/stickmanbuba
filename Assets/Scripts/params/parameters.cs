﻿using UnityEngine;
public static class parameters
{
    public static float speed = 0.5f * Time.deltaTime;
    public static float delta = 0.01f;
    public static float jump = 1f;
    public static int levelsCount = 20;
}