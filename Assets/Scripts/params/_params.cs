﻿using UnityEngine;

public class _params : MonoBehaviour
{
    public int levelNo;
    public int maxJump;
    // movement
    public float multSpeed;
    // lines
    public float percOfNarrowLines;            
    public float percOfBrokenLines;           
    public int noOfLinesInOneRow;
    // gifts
    public int giftNo;   // 0 arrow, 1 clock, 2 magnet, 99 none, 100 random
    // hint
    public int hintNo;
    // items
    public int itemTarget;
    public float percOfStars;
    public float percOfHearts;
    public bool anyItems;
    public bool blankItems;
    public bool blackItems;
    // buba
    public GameObject buba;

    // not to fill
    public float speed;
    private void Start()
    {
        speed = parameters.speed * multSpeed;
    }
}
