﻿public static class tags
{
    // GameObject tags
    public static string line = "line";
    public static string lineHere = "lineHere";
    public static string itemHere = "itemHere";
    public static string presentHere = "presentHere";
    public static string item_flower = "item_flower";
    public static string item_star = "item_star";
    public static string item_heart = "item_heart";
    public static string itemBasket_flower = "itemBasket_flower";
    public static string itemBasket_star = "itemBasket_star";
    public static string itemBasket_heart = "itemBasket_heart";
    public static string gift = "gift";
    public static string gift_arrow = "gift_arrow";
    public static string gift_clock = "gift_clock";
    public static string gift_magnet = "gift_magnet";
    public static string giftBasket_arrow = "giftBasket_arrow";
    public static string giftBasket_clock = "giftBasket_clock";
    public static string giftBasket_magnet = "giftBasket_magnet";
    public static string buba = "buba";
    public static string hint = "hint";
    public static string stop = "stop";
    public static string target1 = "target1";
    public static string target2 = "target2";
    public static string playButton = "playButton";
    public static string canvas = "canvas";
    public static string menuCanvas = "menuCanvas";
    public static string volSlider = "volSlider";
    public static string wallLeft = "wallLeft";
    public static string wallRight = "wallRight";
    public static string ceiling = "ceiling";
    public static string ground = "ground";
    public static string nextLine = "nextLine";

    // player prefs keys
    public static string levelPlayedKey = "levelPlayed";
    public static string levelScoreKey = "levelScore";
    public static string hintKey = "hint";
    public static string musicKey = "music";
    public static string soundKey = "sound";
    public static string muteKey = "mute";
    public static string arrowBasketKey = "arrowBasket";
    public static string clockBasketKey = "clockBasket";
    public static string magnetBasketKey = "magnetBasket";
}
