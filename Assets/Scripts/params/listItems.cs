﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class listItems : MonoBehaviour
{
    [SerializeField] Sprite faceBlank;
    [SerializeField] Sprite faceHappy;
    [SerializeField] Sprite faceSad;
    [SerializeField] GameObject listItem;
    private int itemsOnPage = 9;
    private int pageNum;
    private GameObject buttonLeft;
    private GameObject buttonRight;
    private int currPage;

    private void Start()
    {
        buttonLeft = gameObject.transform.parent.GetChild(0).gameObject;
        buttonRight = gameObject.transform.parent.GetChild(1).gameObject;
        GameObject page = new GameObject();

        for (int i = 0; i < parameters.levelsCount; i++)
        {
            if ( i % itemsOnPage == 0)
            {
                page = new GameObject();
                pageNum = i / itemsOnPage + 1;
                page.name = "page" + pageNum.ToString();
                page.transform.parent = gameObject.transform;
                page.transform.localPosition = Vector3.zero;
            }

            int num = i + 1;
            int itemNum = i % itemsOnPage;

            // read player prefs
            int levelPlayed = PlayerPrefs.GetInt(tags.levelPlayedKey + num.ToString());
            int levelScore = PlayerPrefs.GetInt(tags.levelScoreKey + num.ToString());

            // read GameObjects to fill
            GameObject item = Instantiate(listItem, Vector3.zero, Quaternion.identity);
            item.name = "item" + num.ToString();
            item.transform.parent = page.transform;
            item.transform.localScale = listItem.transform.localScale;
            float yPos = (float)-itemNum / 1.5f;
            Vector3 pos = new Vector3(0, yPos, 0);
            item.GetComponent<RectTransform>().anchoredPosition = pos;
            GameObject hash = item.transform.GetChild(0).gameObject;
            GameObject levelNo = item.transform.GetChild(1).gameObject;
            GameObject score = item.transform.GetChild(2).gameObject;
            GameObject faceImg = item.transform.GetChild(3).gameObject;
            GameObject button = item.transform.GetChild(4).gameObject;

            // set levelNo
            levelNo.GetComponent<Text>().text = num.ToString();
            
            // if level not played yet
            if (levelPlayed == 0)
            {
                // level shadowed
                Color levelCol = levelNo.GetComponent<Text>().color;
                levelCol.a = 0.5f;
                levelNo.GetComponent<Text>().color = levelCol;
                hash.GetComponent<Text>().color = levelCol;

                // score blanked
                score.GetComponent<Text>().text = "";

                // face blanked
                faceImg.GetComponent<Image>().sprite = faceBlank;
                Color faceCol = faceImg.GetComponent<Image>().color;
                faceCol.a = 0.5f;
                faceImg.GetComponent<Image>().color = faceCol;

                // button inactive
                button.GetComponent<Button>().interactable = false;
            }
            // if level has been played
            else
            {
                // if completed
                if (levelScore > 0)
                {
                    score.GetComponent<Text>().text = levelScore.ToString();
                    faceImg.GetComponent<Image>().sprite = faceHappy;
                    button.GetComponent<Button>().onClick.AddListener(() => SceneManager.LoadScene("Level" + num.ToString()));
                }
                // or not
                else
                {
                    score.GetComponent<Text>().text = "";
                    faceImg.GetComponent<Image>().sprite = faceSad;
                    button.GetComponent<Button>().onClick.AddListener(() => SceneManager.LoadScene("Level" + num.ToString()));
                }
            }
        }

        currPage = PlayerPrefs.GetInt(tags.levelPlayedKey) / itemsOnPage;
        showPage(currPage);
    }
    public void arrowLeft()
    {
        currPage = System.Math.Max(currPage-1,0);
        showPage(currPage);
    }

    public void arrowRight()
    {
        currPage = System.Math.Min(currPage + 1, parameters.levelsCount/itemsOnPage);
        showPage(currPage);
    }

    private void showPage(int pageNo)
    {
        // show current page
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            gameObject.transform.GetChild(i).gameObject.SetActive(false);
        }
        gameObject.transform.GetChild(pageNo).gameObject.SetActive(true);

        // if first page, left button inactive
        if (currPage == 0)
            buttonLeft.GetComponent<Button>().interactable = false;
        else
            buttonLeft.GetComponent<Button>().interactable = true;

        // if last page, right button inactive
        if (currPage == parameters.levelsCount / itemsOnPage)
            buttonRight.GetComponent<Button>().interactable = false;
        else
            buttonRight.GetComponent<Button>().interactable = true;
    }
}
