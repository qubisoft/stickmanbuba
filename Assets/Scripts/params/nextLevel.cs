﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class nextLevel : MonoBehaviour
{
    public void next()
    {
        string currentLevel = SceneManager.GetActiveScene().name;
        int currLevNo = int.Parse(currentLevel.Substring("Level".Length));
        int nextLevNo = currLevNo + 1;
        SceneManager.LoadScene("Level"+nextLevNo.ToString());
    }
}