﻿using System.Collections;
using UnityEngine;

public class bubaScript : MonoBehaviour
{
	public GameObject _params;
	public GameObject jumpsCounter;
	public GameObject basketFlower;
	public GameObject basketStar;
	public GameObject basketHeart;

	private float jumpForce = 3f;
	private Vector2 touchPosition;

	private Vector2 wallLeftPos;
	private Vector2 wallRightPos;

	private bool flowerBasketFull = false;
	private bool starBasketFull = false;
	private bool heartBasketFull = false;

	public bool stop = false;
	private bool oneJump = true;

	private void Start()
	{
		wallLeftPos = GameObject.FindGameObjectWithTag(tags.wallLeft).gameObject.transform.position;
		wallRightPos = GameObject.FindGameObjectWithTag(tags.wallRight).gameObject.transform.position;
	}
	void Update()
	{
		if (!stop)
		{
			if (oneJump && Input.touchCount > 0)
			{
				oneJump = false;
				// if touch between the menus
				float xTouch = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position).x;
				if (xTouch > wallLeftPos.x && xTouch < wallRightPos.x)
				{
					if (Input.GetTouch(0).phase == TouchPhase.Began)
					{
						touchPosition = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);

						// rotate buba if needed
						Quaternion rot = gameObject.transform.rotation;
						if (touchPosition.x > gameObject.transform.position.x)
							rot.y = 0;
						else
							rot.y = 180;
						gameObject.transform.rotation = rot;

						// jump
						StartCoroutine(AnimateJump(gameObject, touchPosition, jumpForce));
						jumpsCounter.GetComponent<jumpsCounter>().jump = true;
						//gameObject.transform.GetChild(0).GetComponent<Animator>().SetBool("jumping", true);
					}
				}
			}

			// check if win
			flowerBasketFull = basketFlower.GetComponent<itemBasket>().dontAdd;
			starBasketFull = basketStar.GetComponent<itemBasket>().dontAdd;
			heartBasketFull = basketHeart.GetComponent<itemBasket>().dontAdd;
			if (flowerBasketFull && starBasketFull && heartBasketFull)
			{
				stop = true;
				GameObject resultPrefab = jumpsCounter.GetComponent<jumpsCounter>().resultWindow;
				GameObject resultWin = Instantiate(resultPrefab, resultPrefab.transform.position, Quaternion.identity);
				resultWin.name = "resultWin";
				resultWin.transform.parent = jumpsCounter.transform.parent;
				resultWin.transform.GetChild(0).GetComponent<resultWindow>().resultPoints = jumpsCounter.GetComponent<jumpsCounter>().score;
			}
		}
		else
		{
			Destroy(gameObject);
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.name == "item")
			collision.gameObject.GetComponent<itemScript>().hit = true;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == tags.stop)
		{
			GameObject resultPrefab = jumpsCounter.GetComponent<jumpsCounter>().resultWindow;
			GameObject resultWin = Instantiate(resultPrefab, resultPrefab.transform.position, Quaternion.identity);
			resultWin.name = "resultWin";
			resultWin.transform.parent = jumpsCounter.transform.parent;
			resultWin.transform.GetChild(0).GetComponent<resultWindow>().resultPoints = 0;
		}
	}

	private IEnumerator AnimateJump(GameObject go, Vector2 targetPos, float height, float time = 1.0f)
	{ //parabola animation
		Vector3 basePos = go.transform.position;
		float x1 = basePos.x;
		float x2 = targetPos.x;
		float distance = x2 - x1;
		//float sign = (x2 - x1) / distance;
		float x3 = (x1 + x2) / 2.0f;
		float a = height / ((x3 - x1) * (x3 - x2));

		for (float passed = 0.0f; passed < time;)
		{
			passed += Time.deltaTime;
			float f = passed / time;
			if (f > 1) f = 1;

			float x = x1 + distance * f;
			float y = a * (x - x1) * (x - x2);
			Vector2 tempPos = go.transform.position;
			tempPos.x = x;
			tempPos.y = basePos.y + y;
			go.transform.position = tempPos;

			yield return 0;
		}

		oneJump = true;
	}
}
