﻿using System.Collections;
using UnityEngine;

public class bubaMenu : MonoBehaviour
{
    private bool start;
    private float step;
    private GameObject stop;
    private GameObject target;
    private GameObject target1;
    private GameObject target2;
    private GameObject buba;

    private void Start()
    {
        // run buba
        start = true;
        step = parameters.speed * Time.deltaTime;
        stop = GameObject.FindGameObjectWithTag(tags.stop);
        target1 = GameObject.FindGameObjectWithTag(tags.target1);
        target2 = GameObject.FindGameObjectWithTag(tags.target2);
        target = target2;
        buba = gameObject.transform.GetChild(0).gameObject;
        buba.GetComponent<Animator>().SetBool("running", true);

        // set player prefs if don't exist
        //      level player prefs
        for (int i = 0; i < parameters.levelsCount; i++)
        {
            if (!PlayerPrefs.HasKey(tags.levelPlayedKey + (i+1).ToString()))
            {
                PlayerPrefs.SetInt(tags.levelPlayedKey + (i + 1).ToString(), 0);
                PlayerPrefs.SetFloat(tags.levelScoreKey + (i + 1).ToString(), 0);
            }
        }
        //      settings player prefs
        if (!PlayerPrefs.HasKey(tags.musicKey))
        {
            PlayerPrefs.SetInt(tags.musicKey, 1);
            PlayerPrefs.SetInt(tags.hintKey, 1);
            PlayerPrefs.SetFloat(tags.soundKey, 0.5f);
            PlayerPrefs.SetInt(tags.muteKey, 1);
        }
        //      baskets 
        if (!PlayerPrefs.HasKey(tags.arrowBasketKey))
        {
            PlayerPrefs.SetInt(tags.arrowBasketKey, 0);
            PlayerPrefs.SetInt(tags.clockBasketKey, 0);
            PlayerPrefs.SetInt(tags.magnetBasketKey, 0);
        }
        PlayerPrefs.Save();
    }
    private void Update()
    {
        if (start)
            gameObject.transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == tags.stop)
            StartCoroutine(beHappy());
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == tags.target2)
        {
            Quaternion rot = buba.transform.rotation;
            target = target1;
            rot.y = 180;
            buba.transform.rotation = rot;
        }

        if (collision.gameObject.tag == tags.target1)
        {
            Quaternion rot = buba.transform.rotation;
            target = target2;
            rot.y = 0;
            buba.transform.rotation = rot;
        }

    }
    private IEnumerator beHappy()
    {
        start = false;
        buba.GetComponent<Animator>().SetBool("running", false);
        buba.GetComponent<Animator>().SetBool("happy", true);
        yield return new WaitForSeconds(1.4f);
        start = true;
        buba.GetComponent<Animator>().SetBool("happy", false);
        buba.GetComponent<Animator>().SetBool("running", true);
    }

    private float distance(GameObject bubaObj, GameObject target)
    {
        return Vector3.Distance(bubaObj.transform.position, target.transform.position);
    }
}
