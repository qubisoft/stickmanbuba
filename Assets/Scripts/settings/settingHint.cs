﻿using UnityEngine;
using UnityEngine.UI;

public class settingHint : MonoBehaviour
{
    [SerializeField] Sprite buttonON;
    [SerializeField] Sprite buttonOFF;
    private Image btnImg;
    private string key;

    private void Start()
    {
        key = tags.hintKey;
        btnImg = gameObject.GetComponent<Image>();

        if (PlayerPrefs.GetInt(key) == 0)
            btnImg.sprite = buttonOFF;
        else
            btnImg.sprite = buttonON;
    }
    public void click()
    {
        // change sprite
        if (PlayerPrefs.GetInt(key) == 0)
            btnImg.sprite = buttonON;
        else
            btnImg.sprite = buttonOFF;

        // change player prefs - if 0 set to 1, if 1 set to 0
        PlayerPrefs.SetInt(key, -1 * PlayerPrefs.GetInt(key) + 1);
        PlayerPrefs.Save();
    }
}
