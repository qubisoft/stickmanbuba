﻿using UnityEngine;
using UnityEngine.UI;

public class settingSound : MonoBehaviour
{
    [SerializeField] GameObject sliderText;
    private Slider slider;
    private GameObject fill;
    private GameObject background;
    private Color txtColor;

    private void Start()
    {
        // read objects
        slider = gameObject.GetComponent<Slider>();
        fill = gameObject.transform.GetChild(0).GetChild(0).gameObject;
        background = gameObject.transform.GetChild(1).gameObject;
        txtColor = sliderText.GetComponent<Text>().color;

        // set up slider
        slider.value = PlayerPrefs.GetFloat(tags.soundKey);
        sliderText.GetComponent<Text>().text = System.Math.Round(100 * slider.value, 0).ToString() + "%";

        if (PlayerPrefs.GetInt(tags.muteKey) == 0)
        {
            slider.interactable = false;
            sliderText.GetComponent<Text>().color = new Color(txtColor.r, txtColor.g, txtColor.b, 0.5f);
            fill.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
            background.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
        }

    }

    public void moveSlider()
    {
        PlayerPrefs.SetFloat(tags.soundKey, slider.value);
        sliderText.GetComponent<Text>().text = System.Math.Round(100 * slider.value, 0).ToString() + "%";
    }

    public void changeMute()
    {
        if (PlayerPrefs.GetInt(tags.muteKey) == 0)
        {
            slider.interactable = false;
            sliderText.GetComponent<Text>().color = new Color(txtColor.r, txtColor.g, txtColor.b, 0.5f);
            fill.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
            background.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
        }
        else
        {
            slider.interactable = true;
            sliderText.GetComponent<Text>().color = new Color(txtColor.r, txtColor.g, txtColor.b, 1f);
            fill.GetComponent<Image>().color = new Color(1, 1, 1, 1f);
            background.GetComponent<Image>().color = new Color(1, 1, 1, 1f);
        }
    }
}
