﻿using UnityEngine;

public class pause : MonoBehaviour
{
    public bool pauseBool = false;

    public void click()
    {
        if (pauseBool)
            Time.timeScale = 1;
        else
            Time.timeScale = 0;

        pauseBool = !pauseBool;
    }
}
