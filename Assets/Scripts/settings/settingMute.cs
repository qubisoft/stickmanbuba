﻿using UnityEngine;
using UnityEngine.UI;

public class settingMute : MonoBehaviour
{
    [SerializeField] Sprite buttonON;
    [SerializeField] Sprite buttonOFF;
    [SerializeField] GameObject music;
    [SerializeField] GameObject sound;
    private Image btnImg;
    private string key;

    private void Start()
    {
        key = tags.muteKey;
        btnImg = gameObject.GetComponent<Image>();

        if (PlayerPrefs.GetInt(key) == 0)
            btnImg.sprite = buttonOFF;
        else
            btnImg.sprite = buttonON;
    }
    public void click()
    {
        // change sprite
        if (PlayerPrefs.GetInt(key) == 0)
            btnImg.sprite = buttonON;
        else
            btnImg.sprite = buttonOFF;

        // change player prefs - if 0 set to 1, if 1 set to 0
        PlayerPrefs.SetInt(key, -1 * PlayerPrefs.GetInt(key) + 1);
        PlayerPrefs.Save();

        // update music/sound if exist
        if (music != null)
            music.GetComponent<settingMusic>().changeMute();
        if (sound != null)
            sound.GetComponent<settingSound>().changeMute();
    }
}
