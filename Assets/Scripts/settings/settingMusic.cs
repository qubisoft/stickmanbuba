﻿using UnityEngine;
using UnityEngine.UI;

public class settingMusic : MonoBehaviour
{
    [SerializeField] Sprite buttonON;
    [SerializeField] Sprite buttonOFF;
    private Image btnImg;
    private string key;

    private void Start()
    {
        key = tags.musicKey;
        btnImg = gameObject.GetComponent<Image>();

        if (PlayerPrefs.GetInt(key) == 0)
            btnImg.sprite = buttonOFF;
        else
            btnImg.sprite = buttonON;

        if (PlayerPrefs.GetInt(tags.muteKey) == 0)
        {
            btnImg.sprite = buttonOFF;
            gameObject.GetComponent<Button>().interactable = false;
        }
    }
    public void click()
    {
        // change sprite
        if (PlayerPrefs.GetInt(key) == 0)
            btnImg.sprite = buttonON;
        else
            btnImg.sprite = buttonOFF;

        // change player prefs - if 0 set to 1, if 1 set to 0
        PlayerPrefs.SetInt(key, -1 * PlayerPrefs.GetInt(key) + 1);
        PlayerPrefs.Save();
    }

    public void changeMute()
    {
        if (PlayerPrefs.GetInt(tags.muteKey) == 0)
        {
            btnImg.sprite = buttonOFF;
            gameObject.GetComponent<Button>().interactable = false;
        }
        else
        {
            if (PlayerPrefs.GetInt(key) == 0)
                btnImg.sprite = buttonOFF;
            else
                btnImg.sprite = buttonON;

            gameObject.GetComponent<Button>().interactable = true;
        }
    }    
}