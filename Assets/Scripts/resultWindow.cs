﻿using UnityEngine;
using UnityEngine.UI;

public class resultWindow : MonoBehaviour
{
    [SerializeField] Sprite titleLost;
    [SerializeField] GameObject title;
    [SerializeField] GameObject points;
    [SerializeField] GameObject tryAgain;
    [SerializeField] GameObject top;
    [SerializeField] GameObject buttonNext;
    [SerializeField] GameObject buttonRepeat;
    [SerializeField] GameObject buba;
    public int resultPoints;

    private void Start()
    {
        // set up window
        if (resultPoints > 0)
        {
            points.SetActive(true);
            points.GetComponent<Text>().text = "" + resultPoints;
            tryAgain.SetActive(false);
            buttonNext.SetActive(true);
            buttonRepeat.SetActive(false);
            buba.GetComponent<Animator>().SetBool("win", true);
            //top
        }
        else
        {
            title.GetComponent<Image>().sprite = titleLost;
            points.SetActive(false);
            tryAgain.SetActive(true);
            buttonNext.SetActive(false);
            buttonRepeat.SetActive(true);
            top.SetActive(false);
            buba.GetComponent<Animator>().SetBool("win",false);
        }

        // clean lines, items and buba
        cleanIt(tags.line, "line");
        cleanIt(tags.item_flower, "item");
        cleanIt(tags.item_heart, "item");
        cleanIt(tags.item_star, "item");
        cleanIt(tags.buba, "buba");
    }

    private void cleanIt(string tagName, string type)
    {
        GameObject[] items = GameObject.FindGameObjectsWithTag(tagName);
        foreach (GameObject item in items)
        {
            switch (type)
            {
                case "line":
                    item.GetComponent<lineScript>().stop = true;
                    break;
                case "item":
                    item.GetComponent<itemScript>().stop = true;
                    break;
                case "buba":
                    item.GetComponent<bubaScript>().stop = true;
                    break;
            }
        }
    }
}
