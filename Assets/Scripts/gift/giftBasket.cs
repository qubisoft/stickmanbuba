﻿using UnityEngine;
using UnityEngine.UI;

public class giftBasket : MonoBehaviour
{
    [SerializeField] string type;
    public int itemInBasket;

    private void Start()
    {

        itemInBasket = PlayerPrefs.GetInt(type);
        gameObject.transform.GetChild(0).GetComponent<Text>().text = itemInBasket.ToString();
        
        
    }
}
