﻿using UnityEngine;
using UnityEngine.UI;

public class putGiftHere : MonoBehaviour
{
    public _params _params;
    [SerializeField] GameObject gift;

    private float startY = 2.5f;
    private int numOfX = 4;
    private float[] startXall;

    private Vector2 pos;
    private int time;
    public int jumps;

    private int giftType;

    void Start()
    {
        giftType = _params.giftNo;

        if (giftType != 99)
        {
            // create starting points
            float startWidth = gameObject.GetComponent<BoxCollider2D>().size.x;
            float startX = gameObject.transform.position.x - startWidth / 2;
            startXall = new float[numOfX];
            for (int i = 0; i < numOfX; i++)
                startXall[i] = startX + startWidth / (numOfX - 1) * i;

            // random time to show
            time = Random.Range(10, _params.maxJump);

            // random x position
            pos = new Vector2(startXall[(int)Random.Range(0, numOfX)], startY);
        }
    }
    private void Update()
    {
        if (giftType != 99 && jumps == time)
            putGift();
    }
    private void putGift()
    {
        // put game object in random position
        GameObject item = Instantiate(gift, pos, Quaternion.identity);
        item.name = "gift";

        if (giftType == 100)
            item.GetComponent<giftScript>().giftType = Random.Range(0, 3);
        else
            item.GetComponent<giftScript>().giftType = giftType;
    }
}
