﻿using UnityEngine;
using UnityEngine.UI;   

public class jumpsCounter : MonoBehaviour
{
    [SerializeField] GameObject _params;
    public GameObject resultWindow;

    public bool jump = false;

    private int count;
    private int total;
    private Image fill;
    private Text countTxt;
    private bool onlyOnce = true;

    public int score;

    void Start()
    {
        fill = gameObject.transform.GetChild(1).GetComponent<Image>();
        fill.fillAmount = 1;
        countTxt = gameObject.transform.GetChild(2).GetComponent<Text>();
        total = _params.GetComponent<_params>().maxJump;
        count = total;
        countTxt.text = count.ToString() ;
    }

    void Update()
    {
        score = count;
        if (jump && count != 0)
        {
            count -= 1;
            countTxt.text = count.ToString();
            fill.fillAmount = (float)count / total;
            jump = false;
        }
        if (onlyOnce && count == 0)
        {
            onlyOnce = false;
            GameObject resultWin = Instantiate(resultWindow, resultWindow.transform.position, Quaternion.identity);
            resultWin.name = "resultWin";
            resultWin.transform.parent = gameObject.transform.parent;
            resultWin.GetComponent<resultWindow>().resultPoints = 0;
        }
    }
}
