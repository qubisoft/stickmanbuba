﻿using UnityEngine;
using UnityEngine.UI;

public class itemBasket : MonoBehaviour
{
    private int count = 0;
    private int target;
    public bool dontAdd = false;
    
    private void Start()
    {
        gameObject.transform.GetChild(0).GetComponent<Text>().text = "0";
    }
    public void setTarget(int itemTotal)
    {
        target = itemTotal;
        gameObject.transform.GetChild(2).GetComponent<Text>().text = target.ToString();
        if (target == 0)
            dontAdd = true;
    }
    public void addItem()
    {
        count += 1;
        gameObject.transform.GetChild(0).GetComponent<Text>().text = count.ToString();
        gameObject.GetComponent<Animator>().SetBool("collect", true);
        //gameObject.GetComponent<Animator>().SetBool("collect", false);
        if (count == target)
        {
            dontAdd = true;
            gameObject.GetComponent<Animator>().SetBool("full", true);
        }
    }

    public void cleanBasket(int itemTotal)
    {
        gameObject.GetComponent<Animator>().SetBool("clean", true);
        gameObject.transform.GetChild(0).GetComponent<Text>().text = "0";
        setTarget(itemTotal);
    }

    public void endAnimation(string trigger)
    {
        gameObject.GetComponent<Animator>().SetBool(trigger, false);
    }    
}
