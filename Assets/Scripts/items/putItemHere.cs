﻿using UnityEditor.U2D.Sprites;
using UnityEngine;
using UnityEngine.UI;

public class putItemHere : MonoBehaviour
{
    public _params _params;
    [SerializeField] ItemSO[] ItemSO;
    [SerializeField] GameObject basketFlower;
    [SerializeField] GameObject basketStar;
    [SerializeField] GameObject basketHeart;

    private ItemSO[] items;
    private float startY = 5f;
    private int numOfX = 4;
    private int total = 100;
    private float[] startXall;

    private float blankPerc = 0.1f;
    private float blackPerc = 0.1f;

    public int noOfItems;

    void Start()
    {
        // create starting points
        float startWidth = gameObject.GetComponent<BoxCollider2D>().size.x;
        float startX = gameObject.transform.position.x - startWidth / 2;
        startXall = new float[numOfX];
        for (int i = 0; i < numOfX; i++)
            startXall[i] = startX + startWidth / (numOfX - 1) * i;

        // get all items
        items = new ItemSO[total];
        int percStars = (int)System.Math.Round(total * _params.percOfStars, 0);
        int percStarsBlank = 0;
        int percStarsBlack = 0;
        int percHearts = (int)System.Math.Round(total * _params.percOfHearts, 0);
        int percHeartsBlank = 0;
        int percHeartsBlack = 0;
        int percFlowers = total - percStars - percHearts;
        int percFlowersBlank = 0;
        int percFlowersBlack = 0;
        if (_params.blankItems)
        {
            percStarsBlank = (int)System.Math.Round(blankPerc * percStars, 0);
            percHeartsBlank = (int)System.Math.Round(blankPerc * percHearts, 0);
            percFlowersBlank = (int)System.Math.Round(blankPerc * percFlowers, 0);
            percStars -= percStarsBlank;
            percHearts -= percHeartsBlank;
            percFlowers -= percFlowersBlank;
        }
        if (_params.blackItems)
        {
            percStarsBlack = (int)System.Math.Round(blackPerc * percStars, 0);
            percHeartsBlack = (int)System.Math.Round(blackPerc * percHearts, 0);
            percFlowersBlack = (int)System.Math.Round(blackPerc * percFlowers, 0);
            percStars -= percStarsBlack;
            percHearts -= percHeartsBlack;
            percFlowers -= percFlowersBlack;
        }

        // create bounds: < percStars, < percStars+percStarsBlank etc.
        percStarsBlank += percStars;
        percStarsBlack += percStarsBlank;
        percHearts += percStarsBlack;
        percHeartsBlank += percHearts;
        percHeartsBlack += percHeartsBlank;
        percFlowers += percHeartsBlack;
        percFlowersBlank += percFlowers;
        percFlowersBlack += percFlowersBlank;
        
        for (int i = 0; i < percStars; i++)
            items[i] = ItemSO[0];
        for (int i = percStars; i < percStarsBlank; i++)
            items[i] = ItemSO[1];
        for (int i = percStarsBlank; i < percStarsBlack; i++)
            items[i] = ItemSO[2];
        for (int i = percStarsBlack; i < percHearts; i++)
            items[i] = ItemSO[3];
        for (int i = percHearts; i < percHeartsBlank; i++)
            items[i] = ItemSO[4];
        for (int i = percHeartsBlank; i < percHeartsBlack; i++)
            items[i] = ItemSO[5];
        for (int i = percHeartsBlack; i < percFlowers; i++)
            items[i] = ItemSO[6];
        for (int i = percFlowers; i < percFlowersBlank; i++)
            items[i] = ItemSO[7];
        for (int i = percFlowersBlank; i < percFlowersBlack; i++)
            items[i] = ItemSO[8];

        // put first regular line
        if (_params.anyItems)
        {
            Vector2 pos = new Vector2(startXall[(int)Random.Range(0, numOfX)], startY);
            putItemNow();
            noOfItems = 1;
        }

        // set up baskets
        int starToPick = (int)System.Math.Round(_params.itemTarget * _params.percOfStars, 0);
        int heartToPick = (int)System.Math.Round(_params.itemTarget * _params.percOfHearts, 0);
        int flowerToPick = _params.itemTarget - starToPick - heartToPick;

        basketFlower.GetComponent<itemBasket>().setTarget(flowerToPick);
        basketStar.GetComponent<itemBasket>().setTarget(starToPick);
        basketHeart.GetComponent<itemBasket>().setTarget(heartToPick);
    }
    private void Update()
    {
        if (noOfItems == 0)
            putItemNow();
    }
    public void putItemNow()
    {
        Vector2 pos = new Vector2(startXall[(int)Random.Range(0, numOfX)], startY);
        putItem(items[Random.Range(0, total)], pos);
    }

    private void putItem(ItemSO itemType, Vector2 position)
    {
        // put game object in random position
        GameObject item = new GameObject();
        item.transform.localPosition = position;
        noOfItems += 1;

        // add sprite renderer with line image
        item.AddComponent<SpriteRenderer>();
        item.GetComponent<SpriteRenderer>().sprite = itemType.sprite;

        // add box collider
        item.AddComponent<BoxCollider2D>();
        item.GetComponent<BoxCollider2D>().isTrigger = true;

        // add script
        item.AddComponent<itemScript>();
        item.GetComponent<itemScript>().putItemHere = gameObject;
        switch (itemType.tag)
        {
            case "item_flower":
                item.GetComponent<itemScript>().itemBasket = basketFlower;
                break;
            case "item_heart":
                item.GetComponent<itemScript>().itemBasket = basketHeart;
                break;
            case "item_star":
                item.GetComponent<itemScript>().itemBasket = basketStar;
                break;
        }
        item.GetComponent<itemScript>().itemType = itemType.function;
        item.GetComponent<itemScript>().itemTag = itemType.tag;

        // name, tag, layer
        item.name = "item";
        item.tag = itemType.tag;
        item.GetComponent<SpriteRenderer>().sortingLayerName = itemType.layer;
    }
}
