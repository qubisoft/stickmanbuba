﻿using UnityEngine;

public class itemScript : MonoBehaviour
{
    public GameObject putItemHere;
    public GameObject itemBasket;
    public bool hit = false;
    public int itemType;
    public string itemTag;

    private float startY = 2.5f;
    private float targetY = -4.1f;
    private float dist = 2f;
    private int roundDig = 4;

    private float speed;
    private float multSpeed = 10f;

    private bool goDown = true;
    private bool goBasket = false;

    private Vector2 basketPos;
    private Vector2 scale;

    public bool stop = false;

    private void Start()
    {
        speed = putItemHere.GetComponent<putItemHere>()._params.speed;
        basketPos = itemBasket.transform.position;
        scale = gameObject.transform.localScale;
    }

    private void Update()
    {
        if (!stop)
        {
            Vector2 pos = gameObject.transform.position;
            if (goDown)
                gameObject.transform.position = Vector2.MoveTowards(pos, new Vector2(pos.x, targetY), speed);
            if (goBasket)
            {
                gameObject.transform.position = Vector2.MoveTowards(pos, basketPos, speed * multSpeed);

                // smaller size
                scale.x *= 0.99f;
                scale.y *= 0.99f;
                gameObject.transform.localScale = scale;

                // destroy if go to the basket
                if (System.Math.Round(Vector2.Distance(gameObject.transform.position, basketPos), 2) == 0)
                {
                    itemBasket.GetComponent<itemBasket>().addItem();
                    putItemHere.GetComponent<putItemHere>().noOfItems -= 1;
                    Destroy(gameObject);
                }
            }

            // if goes down to target1, create new one
            if (System.Math.Round(pos.y - (startY - dist), roundDig) == 0)
                putItemHere.GetComponent<putItemHere>().putItemNow();

            // if goes down to target2, destroy it
            if (System.Math.Round(pos.y - targetY, roundDig) == 0)
            {
                putItemHere.GetComponent<putItemHere>().noOfItems -= 1;
                Destroy(gameObject);
            }

            // if item is picked up, modify the basket
            if (!itemBasket.GetComponent<itemBasket>().dontAdd && hit && itemType == 1)
            {
                goBasket = true;
                goDown = false;
                hit = false;
            }

            // blank item
            if (hit && itemType == 2)
            {
                GameObject[] allItems = GameObject.FindGameObjectsWithTag(itemTag);
                foreach (GameObject item in allItems)
                    Destroy(item);
                putItemHere.GetComponent<putItemHere>().noOfItems = 0;
            }

            // black item
            if (hit && itemType == 3)
            {
                _params @params = putItemHere.GetComponent<putItemHere>()._params;
                int starToPick = (int)System.Math.Round(@params.itemTarget * @params.percOfStars, 0);
                int heartToPick = (int)System.Math.Round(@params.itemTarget * @params.percOfHearts, 0);
                int flowerToPick = @params.itemTarget - starToPick - heartToPick;
                switch (itemTag)
                {
                    case "item_flower":
                        itemBasket.GetComponent<itemBasket>().cleanBasket(flowerToPick);
                        break;
                    case "item_heart":
                        itemBasket.GetComponent<itemBasket>().cleanBasket(heartToPick);
                        break;
                    case "item_star":
                        itemBasket.GetComponent<itemBasket>().cleanBasket(starToPick);
                        break;
                }

                goDown = false;
                hit = false;
                putItemHere.GetComponent<putItemHere>().noOfItems -= 1;
                Destroy(gameObject);
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
