﻿using UnityEngine;

[CreateAssetMenu(fileName = "LineSO", menuName = "ScriptableObjects/Line", order = 1)]
public class LineSO : ScriptableObject
{
    public Sprite line;
    public string tag = "line";
    public string layer = "lines";
    public bool shortLine;                  // false - line in base size, true - shorter line (0.5 * base size)
    public bool broken;                     // true - third type of line is go away after jump 
    public RuntimeAnimatorController anim;
}