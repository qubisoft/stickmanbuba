﻿using UnityEngine;

[CreateAssetMenu(fileName = "GiftSO", menuName = "ScriptableObjects/Gift", order = 1)]
public class GiftSO : ScriptableObject
{
    public Sprite sprite;
    public string tag;
    public string layer = "items";
    public int function; // 1 - jumps x2, 2 - speed/2, 3 - magnet
}