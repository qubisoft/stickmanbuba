﻿using UnityEngine;

[CreateAssetMenu(fileName = "ItemSO", menuName = "ScriptableObjects/Item", order = 1)]
public class ItemSO : ScriptableObject
{
    public Sprite sprite;
    public string tag;
    public string layer = "items";
    public int function; // 1 - item to basket, 2 - items on board disappear, 3 - items in basket disappear
}